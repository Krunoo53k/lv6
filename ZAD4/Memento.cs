﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD4
{
    class Memento
    {
        public string ownerName;
        public string ownerAddress;
        public decimal balance;

        public Memento(string ownerName, string ownerAddress, decimal balance)
        {
            this.ownerAddress = ownerAddress;
            this.ownerName = ownerName;
            this.balance = balance;
        }

        public string OwnerName { get => ownerName; set => ownerName = value; }
        public string OwnerAddress { get => ownerAddress; set => ownerAddress = value; }
        public decimal Balance { get => balance; set => balance = value; }
    }
}
