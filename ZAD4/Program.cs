﻿using System;

namespace ZAD4
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount bankAccount = new BankAccount("Ivo Sanader", "Remetinec 169", 0);
            Memento sanaderMemento = bankAccount.StoreState();
            bankAccount.UpdateBalance(10000000);
            Console.WriteLine(bankAccount.ToString());
            bankAccount.RestoreState(sanaderMemento);
            Console.WriteLine(bankAccount.ToString());
        }
    }
}
