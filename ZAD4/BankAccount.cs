﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Text;

namespace ZAD4
{
    class BankAccount
    {
        private string ownerName;
        private string ownerAddress;

        private decimal balance;

        public BankAccount(string ownerName, string ownerAddress, decimal balance)
        {
            this.ownerName = ownerName;
            this.ownerAddress = ownerAddress;
            this.balance = balance;
        }
        public void ChangeOwnerAddress(string address)
        {
            this.ownerAddress = address;
        }
        public Memento StoreState()
        {
            return new Memento(this.ownerName, this.ownerAddress, this.balance);
        }
        public void RestoreState(Memento memento)
        {
            this.ownerName = memento.ownerName;
            this.ownerAddress = memento.ownerAddress;
            this.balance = memento.balance;
        }
        public void UpdateBalance(decimal amount) { this.balance += amount; }
        public string OwnerName { get { return this.ownerName; } }
        public string OwnerAddress { get { return this.ownerAddress; } }
        public decimal Balance { get { return this.balance; } }
        public override string ToString()
        {
            return "Ime vlasnika: "+this.ownerName+". Adresa: "+this.ownerAddress+". Stanje na racunu: "+this.balance+" para.";
        }
    }
}
