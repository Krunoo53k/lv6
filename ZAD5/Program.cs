﻿using System;

namespace ZAD5
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");
            logger.Log("Ok",MessageType.ALL);
            logger.SetNextLogger(fileLogger);
            logger.Log("Ovo ide u fajl", MessageType.ERROR);
            logger.Log("Da vidimo gdje ce ovo ic", MessageType.INFO);
            logger.Log("Opa, pa ono nije islo u fajl, zato ovo oce", MessageType.WARNING);

        }
    }
}
