﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ZAD5
{
    class FileLogger : AbstractLogger
    {
        private string filePath;
        bool appendText;
        public FileLogger(MessageType messageType, string filePath) : base(messageType)
        {
            this.filePath = filePath;
            appendText = false;
        }
        protected override void WriteMessage(string message, MessageType type)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, appendText))
            {
                writer.WriteLine(message);
                appendText = true;

            }
        }
    }
}
