﻿using System;

namespace ZAD3
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker careTaker = new CareTaker();
            ToDoItem toDoItem = new ToDoItem("Title 1", "Task1", new DateTime(2020, 5, 15));

            careTaker.StoreState(toDoItem.StoreState());
            toDoItem.Rename("Title2");
            toDoItem.ChangeTask("Task2");
            careTaker.StoreState(toDoItem.StoreState());
            toDoItem.Rename("Title3");
            toDoItem.ChangeTask("Task3");
            Console.WriteLine(toDoItem.ToString());
            toDoItem.RestoreState(careTaker.PreviousState());
            Console.WriteLine(toDoItem.ToString());
            toDoItem.RestoreState(careTaker.PreviousState());
            Console.WriteLine(toDoItem.ToString());
            
        }
    }
}
