﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD3
{
    class CareTaker
    {
        Stack<Memento> Mementos;
        public CareTaker()
        {
            Mementos = new Stack<Memento>();
        }
        public Memento PreviousState()
        {
            if (Mementos.Count == 0)
            {
                Console.WriteLine("Ne postoji spremljena prosla vrijednost.");
                return null;
            }
            return Mementos.Pop();
        }
        public void StoreState(Memento memento)
        {
            Mementos.Push(memento);
        }
    }
}
