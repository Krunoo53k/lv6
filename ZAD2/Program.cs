﻿using System;

namespace ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box();
            Product product = new Product("Gandhijeva sandala", 1000000);
            Product product1 = new Product("Janjeci umak", 3);
            box.AddProduct(product);
            box.AddProduct(product1);
            IAbstractIterator iterator = box.GetIterator();
            while(iterator.IsDone==false)
            {
                Console.WriteLine(iterator.Current.ToString());
                iterator.Next();
            }
        }
    }
}
