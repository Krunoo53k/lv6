﻿using System;

namespace ZAD1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note1 = new Note("KUPI", "Kupi toalet papir");
            Note note2 = new Note("Hrvatska", "Veceras igra hrvatska protiv turske");
            Note note3 = new Note("Bojan", "Najbolji Bogdanovic");
            Notebook notebook = new Notebook();
            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.AddNote(note3);
            IAbstractIterator iterator = notebook.GetIterator();
            while (iterator.IsDone == false)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
